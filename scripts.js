
function RemoveCookieFunc(event)
{
    let result = confirm("Are you sure you want to remove this cookie?");

    if (result===false)
    {
        return;
    }
    document.cookie = encodeURIComponent(cookieList[this.id].name) + "=" + encodeURIComponent(cookieList[this.id].value) + "; max-age = -1";
    cookieList.splice(this.id, 1);
    
    SetUpTable();
}

function AddCookieFunc(event)
{
    let name = document.forms.addCookie.elements.cookieName.value;
    let value = document.forms.addCookie.elements.cookieValue.value;
    let age = document.forms.addCookie.elements.cookieAge.value;

    if (name === "" || value === "" || age === "")
    {
        alert("To add a cookie, you need to specify all parameters");
        return;
    }

    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + "; max-age=" + encodeURIComponent(age);
    cookieList.push(new Cookie(name, value, age));

    SetUpTable();
}

function SetUpTable()
{
    // clear the table
    do
    {
        table.childNodes[0].remove();
    } while (table.childNodes.length > 0)

    // insert the headers
    table.insertAdjacentHTML('beforeend',
            `<tr>
                <td><label>Name</label></td>
                <td><label>Value</label></td>
                <td><label>Max age</label></td>
                <td></td>
            </tr>`);
    
    // insert the cookie rows        
    for (let i = 0; i < cookieList.length; i++)
    {
        table.insertAdjacentHTML('beforeend',
            `<tr>
                <td><label>${cookieList[i].name}</label></td>
                <td><label>${cookieList[i].value}</label></td>
                <td><label>${cookieList[i].maxage}</label></td>
                <td><button type="button" id="${i}" class="remove-cookie">Remove Cookie</button></td>
            </tr>`);
    }

    // assign click functions to remove buttons
    let removeCookieBtns = document.querySelectorAll('.remove-cookie');
    for (let i = 0; i < removeCookieBtns.length; i++)
    {
        removeCookieBtns[i].addEventListener("click", RemoveCookieFunc)
    }
}

class Cookie 
{
    constructor(name, value, maxage)
    {
        this.name = name;
        this.value = value;
        this.maxage = maxage;
    }
}

let cookieList = [];

let table = document.querySelector('.display-cookie-table');
document.forms.addCookie.elements.addCookie.addEventListener("click", AddCookieFunc);

// initialize the tables with cookies already present
let initCookieList = document.cookie.split(";");

for (let i = 0; i < initCookieList.length;i++)
{
    var initCookie = initCookieList[i].trim().split("=");
    if (initCookie.length >= 2) {
        cookieList.push(new Cookie(initCookie[0].trim(), initCookie[1].trim(), ""));
    }
}

SetUpTable();


